resource "local_file" "this" {
  content  = "I'm the backend"
  filename = "${path.module}/backend"
}

resource "random_pet" "this" {}

output "favorite_pet" {
  value = random_pet.this.id
}