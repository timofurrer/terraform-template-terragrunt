resource "local_file" "this" {
  content  = "I'm the frontend"
  filename = "${path.module}/frontend"
}

resource "random_pet" "this" {}

output "favorite_pet" {
  value = random_pet.this.id
}