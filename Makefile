.PHONY: lock
lock:
	terragrunt run-all providers lock -platform=linux_amd64 -platform=linux_arm64 -platform=darwin_amd64 -platform=darwin_arm64
